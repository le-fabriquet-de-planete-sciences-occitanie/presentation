# Présentation

![logo-fabriquet-planete-sciences-occitanie](Images/logo-fabriquet-planete-sciences-occitanie.jpg)

## LE FABRIQUET

    Batiment CEEI Théogone – Ateliers 1&2
    10 avenue de l’Europe,
    Parc technologique du canal
    31520 Ramonville Saint-Agne

N'hésitez pas à passez en soirée entre 16 et 20h.
- lundi Mécatronique (Papymaker)
- mardi fab'club (13 - 19ans débute en octobre)
- mercredi formation (à réserver sur notre site web bientôt)
- jeudi électronique/robotique/fuseX
- vendredi pas de thèmatique particulière.


La FabTeam
© 2022 Planete Sciences Occitanie

---

<!-- ## Getting started -->
